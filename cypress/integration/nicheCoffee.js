/**
 * get chrome error on navigating to login due to load times and redirect
 * need to move this to before, then navigate to new url after cookies have been established
 */
describe('Visit Niche coffee co UK', () => {
    it('Visit Niche', () => {
      cy.visit('https://www.nichecoffee.co.uk/products/niche-zero?variant=31208685076611')
    })
  })
// if button is disabled no stock exists 
describe('Check "BUY" button is disabled', () => {
    it('Check button', () => {
    cy.get('#button-cart').should('be.disabled')
    })
})
// if 'no stock' text exists, no niche stock
describe('Check "NO STOCK" text exists', () => {
    it('Check text', () => {
    cy.get('.product-info-main').contains('Sorry, no models are in stock at this time')
    })
})
  